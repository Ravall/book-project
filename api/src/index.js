const express = require('express');
const mongoose = require('mongoose')
const axios = require('axios')
const {connectDb} = require("./helpers/db")
const {port, host, db, authApiUrl } = require('./configuration')
const app = express();

const postSchema = new mongoose.Schema({
    name: String
})
const Post = mongoose.model('Post', postSchema)

const startServer = () => {
    app.listen(port, () => {
        console.log(`started api on port ${port}`);
        console.log(`on host ${host}`);
        console.log(`our database ${db}`);

        Post.find(function (err, posts) {
            if (err) return console.error(err)
            console.log(posts)
        })

        const silence = new Post({name: 'test'})
        silence.save(function (err, savedPost) {
            if (err) return console.error(err)
            console.log("saved..", savedPost)
        })
    })
}

app.get('/test', (req, res) => {
    res.send("Our api server is in operation");
})

app.get('/api/testapidata', (req, res) => {
    res.json({
        testwithapi:true
    })
})

app.get('/testwithcurrentuser',  (req, res) => {
    axios.get(authApiUrl + '/currentUser').then(responce => {
        res.json({
            withcurrentuser: true,
            currentUserFromAuth:responce.data
        });
    });

})

connectDb()
    .on("error", console.log)
    .on("disconnected", connectDb)
    .once("open", startServer)
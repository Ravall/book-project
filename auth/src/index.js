const express = require('express');
const axios = require('axios')
const {connectDb} = require("./helpers/db")
const {port, host, db, apiUrl } = require('./configuration')
const app = express();

const startServer = () => {
    app.listen(port, () => {
        console.log(`started auth on port ${port}`);
        console.log(`on host ${host}`);
        console.log(`our database ${db}`);
    })
}

app.get('/test', (req, res) => {
    res.send("Our auth server is in operation");
})

app.get('/testwithapidata', (req, res) => {
    axios.get(apiUrl + '/testapidata').then(response => {
        res.json({
            testapidata: response.data.testwithapi
        })
    });
})

app.get('/api/currentUser', (req, res) => {
    res.json({
        id : '123',
        email: 'foo@gmail.com'
    });
})


connectDb()
    .on("error", console.log)
    .on("disconnected", connectDb)
    .once("open", startServer)